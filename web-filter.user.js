// ==UserScript==
// @author				adair
// @namespace			adair.webfilter
// @name				web-filter
// @include				https://anilinkz.to/*
// @include				https://aniwatcher.com/*
// @include				https://www.ebay.de/*
// @description			Filter page elements and apply css rules (e.g. hightlight, hide).
// @version				1.0
// @require				https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @require				https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js
// @run-at				document-idle
// @grant				none
// @noframes
// ==/UserScript==

// https://code.jquery.com/jquery-2.2.3.min.js
// https://webforms-b553.restdb.io/rest/_jsapi.js

// modules
let lib = window.lib

let jqueryUIStyleURL = 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/vader/jquery-ui.min.css'
// ui-darkness, dark-hive, vader

////////////////////////////////////////////////////////////////////////////////
// main

;(function(){

let _appliedRules = {}
let _data = {}
let _dataFuncs = {}

/* data */

/**
	Key-value storage: string-getter, string-setter, object-getter.
	@param  key     the key
	@param  value   (true): string-getter of value
                    (string): string-setter of value
	@return         if value === (true): (string), else: (object)
*/
function data(key, value) {
	if (_dataFuncs[key]) {
		if (value === true) {
			return localStorage['tfwf.'+key] || ''
		}
		if (value) {
			_data[key] = null
			localStorage['tfwf.'+key] = value
		}
		if (!_data[key]) {
			_data[key] = _dataFuncs[key](data(key, true))
			console.debug(key + ':', _data[key])
		}
		return _data[key]
	}
	throw "data(): unsupported key: " + key
}

_dataFuncs['app-buttons'] = text => text
_dataFuncs['wait-for'] = text => text

_dataFuncs['search-lists'] = function(text) {
	let line, args, name, depth, result = {}
	let lines = text.split('\n')
	let [indents, indent] = lib.listIndent(lines)

	for (let i in lines) {
		line = lines[i].trim()
		if (!line.length)
			continue

		depth = indents[i]
		if (depth == 0) {
			args = line.split(/\t+/g)
			name = args[0]
			result[name] || (result[name] = [])
			if (args[1])
				result[name] = args[1].split(',').map(s => s.trim())
		}
	}
	return result
}

_dataFuncs['filter-config'] = function(text) {
	let line, name, args, depth, obj1, obj2, arr, result = []
	let lines = text.split('\n')
	let [indents, indent] = lib.listIndent(lines)
	let keys = ['func','selSub', 'action', 'rules']

	for (let i in lines) {
		line = lines[i].trim()
		if (!line.length)
			continue

		depth = indents[i]
		if (depth == 0) {
			result[line] || (result[line] = obj1 = {})
		} else if (depth == 1 && obj1) {
			args = line.split(/\t+/g)
			args[3] = obj2 = {}
			keys.forEach((key, idx) => obj1[key] = args[idx])
			if (obj1.selSub == "''")
				obj1.selSub = null
		} else if (depth == 2 && obj2) {
			obj2[line] || ( obj2[line] = arr = [])
		} else if (depth == 3 && arr) {
			arr.push(...line.split(',').map(s => s.trim()))
		}
	}
	return result
}

_dataFuncs['item-actions'] = function(text) {
	let line, type, args, depth, obj1, arr, result = []
	let lines = text.split('\n')
	let [indents, indent] = lib.listIndent(lines)

	for (let i in lines) {
		line = lines[i].trim()
		if (!line.length)
			continue

		depth = indents[i]
		if (depth == 0) {
			result[line] || (result[line] = obj1 = [])
		} else if (depth == 1 && obj1) {
			args = line.split(/\t+/g)
			type = args.shift()
			obj1.push({ type:type, args:args })
		}
	}
	console.debug("--------", result)
	return result
}

function setSearchList(listnames, search, addOrRemove) {
	Array.isArray(listnames) || (listnames = [listnames])
	let d = data('search-lists')
	listnames.forEach(name => d[name] || (d[name] = []))

	let changed = false, text2 = '', fillFunc = lib.fillWS(15)
	for (let name in d) {
		let args = d[name]
		if (listnames.includes(name)) {
			args = lib.arrayToggle(args, search, addOrRemove)
			changed = true
		}
		text2 += fillFunc(name) + args.join(', ') + '\n'
	}
	// console.debug(text2)
	changed && data('search-lists', text2)
}

function addSearchListToFilter(listname, group, filter) {
	let found1, last, j, t, text = data('filter-config', true)

	let text2 = lib.linesByIndent(text, (line, depth, i, lines, indents, indent) => {
		// match group
		if (depth == 1) {
			found1 = line.split(/\t+/g).includes(group)
		}
		// match filter
		else if (depth == 2) {
			if (!found1 || line != filter)
				return

			// lookup the deeper block
			last = lib.lastWithIndent(indents, i, depth+1)
			if (last == j) {
				// no block, add one
				last = ++i
				lines.splice(i, 0, indent.repeat(depth+1))
			} else {
				// jump over the block if we find listname in it
				for (j=i+1; j<=last; j++) {
					if (lines[j].split(',').map(s => s.trim()).includes(listname))
						return last
				}
			}

			// add listname
			lines[last] += (lines[last].trim() == '' ? '' : ', ') + listname
		}
	})
	text.length < text2.length && data('filter-config', text2)
}


/* filter items */

let _filterFuncs = {
	inTitle: function(ele, search) {
		return search ? (ele.title.indexOf(search) != -1) : ele.title
	},
	inText: function(ele, search) {
		return search ? (ele.textContent.indexOf(search) != -1) : ele.textContent
	},
	inId: function(ele, search) {
		return search ? (ele.id.indexOf(search) != -1) : ele.id
	},
}

function doFilter(items, funcName, selSub, searchList, cb) {
	let func = _filterFuncs[funcName]
	if (!func) return
	if (searchList === false)
		return func(selSub ? items.querySelector(selSub) : items)
	return items.filter(node => {
		let target = selSub ? node.querySelector(selSub) : node
		for (let search of searchList) {
			if (func(target, search)) {
				cb && cb(node, search)
				return true
			}
		}
		return false
	})
}

function setItemsFilters(filterConfig, searchLists, itemActions) {
	let selItems, config, nodes, node, rule, lists, name
	for (selItems in filterConfig) {
		config = filterConfig[selItems]
		nodes = Array.from(document.querySelectorAll(selItems))

		if (itemActions[config.action]) {
			nodes.forEach(node => addItemFilterButtons(node, config, searchLists, itemActions))
		}

		for (rule in config.rules) {
			_appliedRules[rule] = true

			lists = config.rules[rule]
			for (name of lists) {
				if (searchLists[name]) {
					doFilter(nodes, config.func, config.selSub, searchLists[name], (node, search) => {
						node.classList.add(rule)
					})
				}
			}
		}
	}
}

function addToAutoDate(search, group, params, callback) {
	let listname = lib.yearMonth()
	setSearchList(listname, search, true)
	if (group && params && params.length) {
		addSearchListToFilter(listname, group, params[0])
	}
}


/* UI */

function filterDialog(params) {
	var $dialog = document.$wfDialog
	if ($dialog) {
		$dialog.params = params
		return $dialog.dialog('open')
	}

	lib.loadStyle(jqueryUIStyleURL)

	document.$wfDialog = $dialog = $(
`<div id="tfwf-dialog" title="Filters">
	<div class="settings">
		<p>Selectors</p>
		<label for="app-buttons-selector">App Buttons</label>
		<input name="app-buttons" type="text" />
		<label for="wait-for-selector">Wait-For</label>
		<input name="wait-for" type="text" />
	</div>
	<div class="fields">
		<p>Config</p>
		<textarea name="filter-config" />
		<p>Item Actions</p>
		<textarea name="item-actions" />
		<p>Search Lists</p>
		<textarea name="search-lists" />
	</div>
	<div class="actions">
		<div>
			<input type="button" value="Save" class="save">
			<input type="button" value="Reload" class="reload">
			<span class="error-message" style="display: none; color:red; " />
		</div>
		<div>
			<label for="search">Add:</label>
			<input type="text" name="search" class="text ui-widget-content ui-corner-all">
		</div>
		<div>
			<label>To:</label>
			<span class="search-list-items" />
		</div>
	</div>
</div>`)

	$dialog.params = params

	$dialog.find('textarea').keydown(lib.enableTextareaTabs)
	$dialog.find('input.reload').click(reloadDialog)
	$dialog.find('input.save').click(save)
	let keys = ['app-buttons', 'wait-for', 'filter-config', 'search-lists', 'item-actions']

	function fields(key, set) {
		let sel = '[name="'+  key +'"]'
		if (set) {
			data(key, $dialog.find(sel).val())
		} else {
			$dialog.find(sel).val(data(key, true))
		}
	}
	function save() {
		keys.forEach(key => fields(key, true))
		reloadDialog()
		applyFilters()
	}
	function reloadDialog() {
		addSearchListListButtons()
		keys.forEach(key => fields(key))
	}
	function searchValue(set) {
		let $ele = $dialog.find('[name=search]')
		return set ? $ele.val(set) : $ele.val()
	}
	function addSearchListListButtons() {
		var $list = $dialog.find('.search-list-items').html('')
		let itemActions = data('item-actions')

		for (let group in itemActions) {
			let actions = itemActions[group]
			for (let action of actions) {
				if (action.type == 'auto-date') {
					let args = action.args
					let text = 'Auto-Date' + (args.length ? ': ' + args[0] : '')
					$list.append($('<span class="btn auto-date"/>').html(text).click(e => {
						let search = searchValue()
						addToAutoDate(search, group, args)
						reloadDialog()
						applyFilters()
					}))
				}
			}
		}

		let searchLists = data('search-lists')
		for (let listname in searchLists) {
			$list.append($('<span class="btn"/>').html(listname).click(e => {
				let search = searchValue()
				if (search) {
					setSearchList(listname, search, true)
					reloadDialog()
					applyFilters()
				}
			}))
		}
	}
	function open() {
		searchValue($dialog.params && $dialog.params.search)
		reloadDialog()
	}

	return $dialog.dialog({ height: '700', minWidth: '800', open: open })
}

let _itemActionFuncs = {}

_itemActionFuncs['dialog'] = function(ele, config, args) {
	if (ele.querySelector('.open-dialog')) return
	return $('<div class="open-dialog" />').click(e => {
		console.debug(ele, config.func, config.selSub)
		let search = doFilter(ele, config.func, config.selSub, false)
		filterDialog({ search:search })
	})
}

_itemActionFuncs['auto-date'] = function(ele, config, args) {
	if (ele.querySelector('.auto-date')) return
	return $('<div class="auto-date" />').click(e => {
		let search = doFilter(ele, config.func, config.selSub, false)
		addToAutoDate(search, config.action, args)
		applyFilters()
	})
}

_itemActionFuncs['toggle'] = function(ele, config, args) {
	let rule = args[0]
	if(ele.querySelector('.toggle.toggle-'+rule)) return
	return $('<div class="toggle toggle-'+rule+'" />').click(e => {
		let listnames, search = doFilter(ele, config.func, config.selSub, false)
		if (rule && (listnames = config.rules[rule])) {
			setSearchList(listnames, search)
			applyFilters()
		}
	})
}

function addItemFilterButtons(ele, config, searchLists, actionButtons) {
	let action, actions = actionButtons[config.action]
	let $target = $(ele).find('.tfwf-item-actions')

	$target.length || ($target = $('<div class="tfwf-item-actions" />').appendTo(ele))

	for (let action of actions) {
		if (_itemActionFuncs[action.type]) {
			let btn = _itemActionFuncs[action.type](ele, config, action.args)
			btn && $target.append(btn)
		}
	}
}


/* app */

function addAppButtons(to) {
	if ($('#tfwf-menu').length)
		return
	$('<div id="tfwf-menu">')
	.append(
		$('<a class="open-dialog">Filter</a>').click(filterDialog),
		$('<a class="reset-filters">Toggle</a>').click(
			e => Object.keys(_appliedRules).length ? stripFilters() : applyFilters()
		)
	)
	.appendTo(to)
}

function stripFilters() {
	Object.keys(_appliedRules).forEach(cls => {
		Array.from(document.querySelectorAll('.'+cls)).forEach(ele => ele.classList.remove(cls))
	})
	_appliedRules = {}
}

function applyFilters() {
	stripFilters()
	setItemsFilters(data('filter-config'), data('search-lists'), data('item-actions'))
}

function init() {
	// ;['A','B','C','D','E'].forEach(s => setSearchList("tests", s, true))
	// ;['B'].forEach(s => setSearchList("tests", s, false))
	// ;[1,2,3].forEach(n => ['D'].forEach(s => setSearchList("tests", s) ) )
	// addSearchListToFilter('TEST', 'default', 'filter-hide')

	// $('main .open-dialog:first, smain .auto-date:first').click()
	// $('main .toggle-filter-gold:first').click()
	// filterDialog()

	let appButtons = data('app-buttons')
	let waitFor = data('wait-for')
	
	if (appButtons && appButtons.length) {
		lib.awaitInterval(10, 100, (timer) => { 
			if (!waitFor ||  $(waitFor).length) {
				clearInterval(timer)
				addAppButtons(appButtons)
				applyFilters()
			}
		})
	} else {
		lib.awaitInterval(1, 1000, (timer) => {
			clearInterval(timer) 
			filterDialog()
		})
	}
}

init()

let self = window.WebFilterApp = {}
;[init].forEach(f => self[f.name] = f)

}());

/*
- toggle rules -> skip-list
- namespaces
*/
