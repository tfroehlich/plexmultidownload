// ==UserScript==
// @author				adair
// @namespace			adair.humblebundle.com
// @name					humblebundle
// @include				https://www.humblebundle.com/home/library
// @description		Extend the library by custom filter.
// @version				1.0
///@require				https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @run-at				document-idle
// @grant					none
// ==/UserScript==

function whileInterval(count, delay, func) {
  var timer = setInterval(function () {
    if (count-- > 0) { if ( func() === true ) return; }
    console.log("clearInterval");
    clearInterval(timer);
  }, delay);
}

function _filter(list) {
  return (function () {
    for (var i = 0; i < list.length; i++)
      // if (list[i].length == 0) continue;
      if (this.innerHTML.indexOf(list[i]) != - 1)
        return true;
    return false;
  })
}

var humbleBundle = {
  filterLists: {
    meh: [
      'Blocks That Matter', 'AR-K', 'Chocolate Castle', 'Dream Machine', 'Dungeon Defenders',
      'Dungeon of Elements', 'Endless Space', 'Gish', 'Jasper', 'Indie Game: The Movie', 'Overruled',
      'FarSky', 'Zen Puzzle'],
    lame: [
      'Deadlight', 'Freedom Planet', 'BIT.TRIP'],
    done: [
      'Sequence', 'Pixel Piracy', 'Intake', 'Basement Collection', 'Cave Story', 'Binding of Isaac',
      'Voxatron', 'Symphony', 'Retro/Grade', 'Lexaloffle', 'Done'],
    books: [
      'Chew Vol', 'East of West Vol', 'Fatale Vol', 'Invincible Vol', 'Lazarus Vol', 'Manhattan Projects',
      'Morning Glories Vol', 'Outland - Special Edition', 'Revival Vol', 'Saga Vol', 
      'Audiobook'],
    worms: [
      'Worms'],
  },
};

function humbleBundleFilters() {
  var _selSEL = 'selected';
  var $ele = $('.top-controls .switch-sort-order');

  function _bindButton(btn, list) {
    var $list = $('.subproducts-holder .subproduct-selector .text-holder h2');
    var $mehList = $list.filter(_filter(list)).closest('.subproduct-selector');
    // var $mehList = $list.filter(_filter(list)).closest('.subproduct-selector');
    
    if ($(btn).toggleClass(_selSEL).hasClass(_selSEL)) {
      $mehList.css('display', 'none')
    } else {
      $mehList.show();
    }
  }
  
  var buttons = '';
  for (var p in humbleBundle.filterLists) {
    buttons += '<span class="'+p+'">'+p+'</span>';
  }
  $ele.append('<span class="filters">'+buttons+'</span>')

  var $buttons = $ele.find(".filters span").attr('style','padding:2px 8px; margin:8px 2px 0 8px; border-radius:5px; background-color:silver');

  function _initLoop(cls, list) { $buttons.filter('.'+ cls).click(function(e){ _bindButton(this, list )}).click(); } 
  for (var p in humbleBundle.filterLists) _initLoop(p, humbleBundle.filterLists[p])
}

whileInterval(10, 800, function(){
  var $ele = $('.top-controls .switch-sort-order');
  var $ele2 = $('.subproducts-holder .subproduct-selector');
  if ($ele.length < 1 && $ele2.length < 1) return true;
  setTimeout(humbleBundleFilters, 100);
});


