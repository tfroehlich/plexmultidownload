// ==UserScript==
// @author				adair
// @namespace			adair.greasemonkey.ref
// @name       		Example Script
// @name:ru		    Пользовательские скрипты
// @description		Webfilter.
// @version				1.0
//
// @include				http://www.example.com/*
// @exclude				http://www.example.com/*
///@match					http://*.example.com/*
//
///@icon					http://www.example.org/icon.png
///@resource			resourceName http://www.example.com/example.png
///@require				http://www.abc.de
///@run-at				document-start document-end document-idle
///@grant					none
///@noframes			script will run only in the top-level document, never in nested frames
// ==/UserScript==