// ==UserScript==
// @author				adair
// @namespace			adair.pretty
// @name				pretty
// @include				https://anilinkz.to/*
// @include				https://aniwatcher.com/*
// @description			Prettify.
// @version				1.0
// @run-at				document-start
// @grant				none
// @noframes
// ==/UserScript==

function prettyJsonCSS(cls) {
	var cls = cls || 'pretty-json';
	if (!document.getElementById(cls)) {
    var ele = document.createElement('div');
		ele.innerHTML = 
`<style type="text/css" id="` + cls + `">
.$ { color: #f8f8f2; }
.$ .string { color: #e6db74; }
.$ .number { color: #ae81ff; }
.$ .boolean { color: #ae81ff; }
.$ .null { color: #ae81ff; }
.$ .key { color: #f92672; }
</style>`.replace(/\$/g, cls) // monokai-sublime
		document.body.appendChild(ele.firstChild);
	}
	return cls;
}

function json(json) {
	// remove indentation from primitive arrays
	json = json.replace(/\[([^\]]+)\]/g, function (match) {
		return match.replace(/,\n/g, ',').replace(/,  +/g, ', ').replace(/\n(.*?)\]/g, ']').replace(/\[\n/g, '[').replace(/\[  +/g, '[');
	});

	json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	json = json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
		function (match) {
			var cls = 'number';
			if (/^"/.test(match)) {
				if (/:$/.test(match)) {
					cls = 'key';
				} else {
					cls = 'string';
				}
			} else if (/true|false/.test(match)) {
				cls = 'boolean';
			} else if (/null/.test(match)) {
				cls = 'null';
			}
			return '<span class="' + cls + '">' + match + '</span>';
		});

	var hlCls = prettyJsonCSS();
	return '<span class="' + hlCls + '">' + json + '</span>';
}


////////////////////////////////////////////////////////////////////////////////
// module interface

window.pretty = {}
window.pretty.prettyJsonCSS = prettyJsonCSS
window.pretty.json = json

console.log("PRETTY END")