2	lib.escapeRegExp()
3	lib.listIndent()
1	lib.linesByIndent()
1	lib.lastWithIndent()
1	lib.yearMonth()
1	lib.loadStyle()
1	lib.whileInterval()


////////////////////////////////////////////////////////////////////////////////
// local storage

function LS(key, obj) {
	try { return _LS(key, obj) } catch (ex) { console.error(ex.message, ex.stack, key, obj) }
}
function _LSGet(key) {
	return localStorage[key] ? JSON.parse(localStorage[key]) : null
}
function _LS(key, obj) {
	key = key || Object.keys(localStorage)

	// get localstorage items by keys
	if (Array.isArray(key)) {
		var result = {}
		for (var k of key) {
			// console.debug(k, typeof localStorage[k], localStorage[k])
			if (localStorage[k]) {
				result[k] = JSON.parse(localStorage[k])
			}
		}
		return result
	}
	// combine or merge items (first level only)
	if (typeof key === 'object') {
		for (var k in key) {
			var o0, o = key[k]
			// remove item
			if (o === null) {
				delete localStorage[k]
				continue
			}
			// merge item?
			if (obj === true && typeof o == 'object' && localStorage[k]) {
				if ((o0 = JSON.parse(localStorage[k])) && typeof o0 == 'object') {
					o = { ...o0, ...o }
				}
			}
			localStorage[k] = JSON.stringify(o)
		}
		return
	}
	// remove item
	if (obj === null) {
		delete localStorage[key]
		return
	}
	// set item
	if (obj !== undefined) {
		localStorage[key] = JSON.stringify(obj)
		return obj
	}

	// get item
	return _LSGet(key)
}

function testLS() {
	var o = { t: {} }, d = +new Date()
	o.t[d] = 1
	lib.LS('t', null)
	// lib.LS(o, true)
	console.log(Object.keys(lib.LS()))
	console.log(lib.LS('t'))
}


////////////////////////////////////////////////////////////////////////////////
// events

function keyup(key) {
	var ev = new KeyboardEvent('keyup', {target:document.body, key:key})
	// var ev = document.createEvent("KeyboardEvent")
	// ev.initKeyEvent("keyup", true, true, window, 0, 0, 0, 0, 13, 13) 
	return document.body.dispatchEvent(ev)
}

////////////////////////////////////////////////////////////////////////////////
// events

function LA(name, set) {
	if (!window.LA)
		window.LA = {}
	if (name) {
		if (set && !window.LA[name])
			window.LA[name] = {}
		return window.LA[name]
	}
	return window.LA
}

function addEvent(name, func, position) {
	// @todo position
	if (typeof func != 'function')
		return false
	var events = LA('events', true)
	if (!events[name])
		events[name] = []
	// console.log(events[name], events[name].indexOf(func))
	if (events[name].indexOf(func) > -1)
		return false
	events[name].push(func)
}

function triggerEvent(name) {
	console.log(name)
	var events = LA('events')
	if (!events)
		return false
	if (!events[name])
		return false
	for (var p in events[name]) {
		try {
			events[name][p]()
		} catch (e) {
			console.warn('error: event ' + name  + '[' + p + ']')
		}
	}
}

var f1 = function() { console.log('blub 1') }
var f2 = function() { console.log('blub 2') }
var f3 = function() { console.log('blub 3') }
//addEvent('before_test1', f1)
//addEvent('after_test1', f2)
//addEvent('test1', f3)
//console.log(LA('events')
// triggerEvent('test')

function test1() {
	console.log('function test1', arguments)
}

function functionName(fun) {
  var ret = fun.toString()
  ret = ret.substr('function '.length)
  ret = ret.substr(0, ret.indexOf('('))
  return ret
}

function bindEvents(fn, unbind) {
	var name = functionName(fn)
	if (name.length == 0) {
		console.warn('ignored function without name')
		return false
	}
	var overrides = LA('functionOverrides', true)
	if (unbind) {
		// console.log(overrides[name].toString())
		if (overrides[name]) {
			eval(name + ' = overrides[name]')
			delete overrides[name]
		}
	} else if (!overrides[name]) {
		eval(name + ' = _bindEvents(fn, name)')
		overrides[name] = fn
		// console.log(overrides[name].toString())
		// console.log(test1.toString())
	}
}

function _bindEvents(fn, name) {
	return new Function('fn',
		"return function " + name + "(){" +
			`if (false === triggerEvent("before_` + name + `"))
				return false
			var result = fn.apply(this,arguments)
			triggerEvent("after_` + name + `")
			return result`
	+   "}"
	)(fn)
}

// test1(12)
// bindEvents(test1)
// test1(34)
// bindEvents(test1, true)
// test1(56)


////////////////////////////////////////////////////////////////////////////////
// debugging

function trace() { console.log.apply(console, arguments) }
// function log() { console.log.apply(console, arguments) }
// function warn() { console.warn.apply(console, arguments) }
// function error() { console.error.apply(console, arguments) }

function caller(level) {
	var stack = '', args = arguments, name
	for (var i=0; i<level; i++) {
		name = args.callee.caller.name.toString()
		stack += (name ? name : 'function()') + '.'
		console.log("-", name, name.length)
		if (args.callee && args.callee.caller) {
			args = args.callee.caller.arguments
		} else {
			break
		}
	}
	return stack
}

