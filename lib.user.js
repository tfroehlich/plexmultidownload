// ==UserScript==
// @author				adair
// @namespace			adair.lib
// @name				lib
// @include				https://anilinkz.to/*
// @include				https://aniwatcher.com/*
// @include				https://www.ebay.de/*
// @description			.
// @version				1.0
// @run-at				document-start
// @grant				none
// @noframes
// ==/UserScript==

////////////////////////////////////////////////////////////////////////////////
// array / objecs

if (!String.prototype.format) {
	String.prototype.format = function() {
		return [...arguments].reduce((p,c) => p.replace(/%s/,c), this)
	}
}

function pick(obj, keys) {
    return Object.assign({}, ...keys.map(k => k in obj ? {[k]: obj[k]} : {}))
}

function arrayToggle(arr, str, addOrRemove) {
	if (addOrRemove === true) {
		if (arr.includes(str))
			return [...arr]
	} else if (addOrRemove === false || arr.includes(str)) {
		return arr.filter(s => s !== str)
	}
	return [...arr, str]
}


////////////////////////////////////////////////////////////////////////////////
// dom

function loadStyle(url) {
	if (document.querySelector('link[href="' + url + '"]')) {
		return new Promise((resolve, reject) => { resolve() })
	}
	return new Promise((resolve, reject) => {
		let link    = document.createElement('link')
		link.type   = 'text/css'
		link.rel    = 'stylesheet'
		link.onload = () => { resolve() }
		link.href   = url
		document.head.appendChild(link)
	})
}

function addHTML(html) {
	var ele = document.createElement('div')
	ele.innerHTML = html
	document.body.appendChild(ele.firstChild)
	delete ele
}

function addStyle(style) {
	addHTML('<style type="text/css">' + style + '</style>')
}


////////////////////////////////////////////////////////////////////////////////
// date

function yearMonth(dt) {
	if (!dt) dt = new Date()
	return dt.getFullYear() + "_" + (dt.getMonth() + 1)
}


////////////////////////////////////////////////////////////////////////////////
// text

function selectText(node) {
	const selection = window.getSelection()
	const range = document.createRange()
	range.selectNodeContents(node)
	selection.removeAllRanges()
	selection.addRange(range)
}

function deselectText() {
	window.getSelection().removeAllRanges()
}

function copyText(node) {
	lib.selectText(node)
	document.execCommand("copy")
	lib.deselectText()
}

function escapeRegExp(string) {
	return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

function fillWS(len, char, div) {
	char || (char = "\t")
	div || (div = char == '\t' ? 4 : 1)
	return (str => str + char.repeat((len-str.length)/div+1))
}


////////////////////////////////////////////////////////////////////////////////
// text indentation

function testIndent(indent, line) {
	let start = line.substring(0, line.length-line.trimStart().length)
	return indent && start.replace(new RegExp(indent, 'g'), '').length ? false : start
}

function listIndent(lines, proceed) {
	let line, len, start, indent, re, depth, result = [], n = 0
	for (line of lines) {
		len = line.search(/\S/)
		start = line.substring(0, len)
		if (len) {
			if (indent) {
				if (start.replace(re, '').length) {
					if (!proceed) {
						console.error(`line ${n}: inconsistent indentation: expected multiple of '${indent}' in '${start}'`)
						return false
					}
					depth = -1
				} else {
					depth = len / indent.length
				}
			} else {
				depth = 1
				indent = start
				re = new RegExp(indent, 'g')
			}
		} else {
			depth = 0
		}
		result.push(depth)
		n++
	}
	return [result, indent]
}

function linesByIndent(text, callback) {
	let lines = text.split('\n')
	let [indents, indent] = lib.listIndent(lines)
	for (let i=0; i<lines.length; i++) {
		line = lines[i].trim()
		if (!line.length)
			continue
		let result = callback(line, indents[i], i, lines, indents, indent)
		if (result === false)
			break
		if (typeof result === 'number')
			i = result
	}
	return lines.join("\n")
}

function lastWithIndent(indents, i, depth, callback) {
	for (; i<indents.length; i++) {
		if (!indents[i+1] || indents[i+1] != depth)
			break;
	}
	return i
}

////////////////////////////////////////////////////////////////////////////////
// processes

function awaitInterval(count, delay, func) {
	const timer = setInterval(() => {
		// console.debug(count)
		;(count-- <= 0 || func(timer) === true) && clearInterval(timer)
	}, delay)
	return timer
}


////////////////////////////////////////////////////////////////////////////////
// events

function enableTextareaTabs(e) {
	var keyCode = e.keyCode || e.which;
	if (keyCode == 9) {
		e.preventDefault();
		var start = this.selectionStart;
		var end = this.selectionEnd;
		var text = $(this).val();
		var selText = text.substring(start, end);
		$(this).val( text.substring(0, start) + "\t" + selText.replace(/\n/g, "\n\t") + text.substring(end) );
		this.selectionStart = this.selectionEnd = start + 1;
	}
}

////////////////////////////////////////////////////////////////////////////////
// special

// may throw cross-site error
function querySelectorAllIframe(iframe, sel) {
	if (typeof iframe == 'string')
		iframe = document.querySelector(iframe)
	return iframe ? iframe.contentWindow.document.body.querySelectorAll(sel) : null
}


////////////////////////////////////////////////////////////////////////////////
// module interface

let self = window.lib = {}
;[pick, arrayToggle, loadStyle, addHTML, addStyle, 
  yearMonth, selectText, deselectText, copyText, escapeRegExp, fillWS, 
  testIndent, listIndent, linesByIndent, lastWithIndent, 
  awaitInterval, enableTextareaTabs, querySelectorAllIframe]
	.forEach(f => self[f.name] = f)

/*
*/