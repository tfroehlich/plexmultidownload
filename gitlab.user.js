// ==UserScript==
// @author				adair
// @namespace			adair.gitlab.com
// @name					gitlab
// @include				https://gitlab.com/kirect/kirectqt/issues
// @description		Sortable issue list.
// @version				1.0
// @grant					GM_addStyle
// @grant					GM_getResourceText
// @grant					none
// @run-at				document-end
// @require				https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @require				https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js
// s@resource			customCSS css/ui-darkness/jquery-ui-1.8.16.custom.css
// ==/UserScript==


// $("head").append(
// 	'<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/dark-hive/jquery-ui.min.css" rel="stylesheet" type="text/css">'
// );

// $("body").append('<div id="gmOverlayDialog"><h1>Sample Dialog added using jQuery-UI</h1></div>');
// $("#gmOverlayDialog").dialog({
// 	modal:false, title:"Title",
// 	position: { my:"top", at:"top", of:document, collision:"none" },
// 	width:"auto", minWidth:400, minHeight:200, zIndex:3666
// }).dialog("widget").draggable("option", "containment", "none");
// $("#gmOverlayDialog").parent().css({ position:"fixed", top:0, left:"4em", width:"75ex"}); // Fix bug in FF

// WARN not working right now
localStorage[projectIssueIdMap] = [];
return;

var projectName = $('.shortcuts-project').attr('href').replace(/\//g, "_");
var projectIssueIdMap = projectName+"_issueIdMap";
var $issuesList = $('.issues-list');

$issuesList.sortable({
	placeholder: "ui-state-highlight",
	update: function( event, ui ) {
		localStorage[projectIssueIdMap] = $.map($(this).find('li'), function(el) { return el.id; })
	}
}).disableSelection();


console.log(localStorage[projectIssueIdMap]);
if (localStorage[projectIssueIdMap]) {
	var $items = $issuesList.children().detach();
	$.each(localStorage[projectIssueIdMap].split(','), function(index, value) {
		$issuesList.append($items.filter("#" + value));
	});
}
