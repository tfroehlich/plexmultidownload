// ==UserScript==
// @author              adair
// @namespace           adair.page-nav
// @name                web-nav
// @include             *
// @description         Extend web pages by custom navigation events (e.g. bind keys to click events next page, go previous page, etc.)
// @run-at              document-idle
// @grant               none
// @noframes
// ==/UserScript==


/*************************************************************************************** utils ****/

function matchURL(urlRegex) {
	var re = new RegExp(urlRegex,"g")
	return window.location.href.match(re)
}

function matchElementProperty(sel, ele, property) {
	while (ele = ele[property]) {
		if (ele.matches(sel))
			return ele
	}
	return null
}

function pick(obj, keys) {
	return Object.assign({}, ...keys.map(k => k in obj ? {[k]: obj[k]} : {}))
}

function __d_keys(ev) {
	if (['Shift', 'Control', 'Alt'].includes(ev.key)) return
	// console.debug(pick(ev, ['type', 'key', 'charCode', 'keyCode', 'ctrlKey', 'shiftKey', 'target']))
	// ev.preventDefault(); ev.stopPropagation()
}

function removeClass(cls) {
	Array.from(document.querySelectorAll('.'+cls)).forEach(ele => ele.classList.remove(cls))
}

function inViewport(ele) {
	var r = ele.getBoundingClientRect()
	return r.top >= 0 && r.left >= 0 && r.bottom <= window.innerHeight && r.right <= window.innerWidth
}

/********************************************************************************** navigation ****/

(function(window, document, undefined){

	/** Public */

	let self = window.Navigation = {}
	;[init,keyToClick,querySelector,nextPaginationItem,prevPaginationItem,navigate,queryFocus,removeFocus,removeHighlight,keyup]
		.forEach(f => self[f.name] = f)

	/* Private */

	var keyConfig = {}

	/* - Callbacks */

	function keyToClick(keys, func, args) {
		// console.debug('keyToClick', keys, func, args)
		if (!(keys && func)) return
		keys = keys.split(',')
		for (let key of Array.isArray(keys) ? keys : [keys]) {
			let sp = key.split(':')
			key = sp[0]
			if (!keyConfig[key])
				keyConfig[key] = []
			let data = {func: func, args: args}
			if (sp[1])
				data.attributes = sp[1].split(',')
			keyConfig[key].push(data)
		}
	}

	function querySelector(selItems, selSub) {
		let ele = document.querySelector(selItems)
		return ele && selSub && selSub != "''" ? ele.querySelector(selSub) : ele
	}

	function prevPaginationItem(selList, selCurr, selItems, selSub) {
		return prevNextPaginationItem(selList, selCurr, selItems, selSub)[0]
	}

	function nextPaginationItem(selList, selCurr, selItems, selSub) {
		return prevNextPaginationItem(selList, selCurr, selItems, selSub)[1]
	}

	function prevNextPaginationItem(selList, selCurr, selItems, selSub) {
		let parent, ele, r
		if (ele = document.querySelector(selCurr)) {
			while (parent = ele.parentNode) {
				if (parent.querySelector(selItems)) {
					r = [matchElementProperty(selItems, ele, 'previousElementSibling'),
						 matchElementProperty(selItems, ele, 'nextElementSibling')]
					if (selSub && selSub !== '') {
						if (r[0]) r[0] = r[0].querySelector(selSub)
						if (r[1]) r[1] = r[1].querySelector(selSub)
					}
					return r
				}
				if (parent.matches(selList))
					break
				ele = parent
			}
		}
		return [null, null]
	}


	/* - Directional element navigation */

	var eleFocus = null

	function queryFocus(selItems, selSub) {
		return querySelector('.tfwn-focus', selSub)
	}

	function removeFocus() {
		removeClass('tfwn-focus')
	}

	function removeHighlight() {
		removeClass('tfwn-focus')
		removeClass('tfwn-highlight')
	}

	function setFocus(ele) {
		if (ele) {
			if (eleFocus)
				eleFocus.classList.remove("tfwn-focus")
			eleFocus = ele
			eleFocus.classList.add("tfwn-focus")
			if (!inViewport(eleFocus))
				eleFocus.scrollIntoView()
			return true
		}
		return false
	}

	let _dir = {
		args: {
			l: r => ({ a:r.x     , b: r.y+(r.height>>1), cmp: false }),
			r: r => ({ a:r.right , b: r.y+(r.height>>1), cmp: true  }),
			u: r => ({ a:r.y     , b: r.x+(r.width>>1) , cmp: false }),
			d: r => ({ a:r.bottom, b: r.x+(r.width>>1) , cmp: true  }),
		},
		params: {
			l: r => ({ t1:r.right , t2:r.y, t3:r.height }),
			r: r => ({ t1:r.x     , t2:r.y, t3:r.height }),
			u: r => ({ t1:r.bottom, t2:r.x, t3:r.width  }),
			d: r => ({ t1:r.y     , t2:r.x, t3:r.width  }),
		},
	}

	function _dist(x1,x2,y1,y2) {
		let tx = x2-x1, ty = y2-y1
		return tx*tx + ty*ty
	}

	function _findNext(dir, list, R, inv) {
		let func = _dir.params[dir]
		let cmp = R.cmp
		let dist = Number.MAX_SAFE_INTEGER
		let ele, result, d
		
		for (ele of list) {
			r = func(ele.getBoundingClientRect())
			if (cmp ? r.t1 > R.a : r.t1 < R.a) {
				// d = _dist(a, r.t1, b, r.t2) + _dist(a, r.t1, b, r.t2+r.t3)
				d = _dist(R.a, r.t1, R.b, r.t2+(r.t3>>1)) 
				if (dist > d) {
					dist = d
					result = ele
				}
			}
		}
		return result
	}

	function navigate(dir, selItems, seLCurrent) {
		// console.debug(dir, selItems, seLCurrent)
		if (!(dir == 'l' || dir == 'r' || dir == 'u' || dir == 'd'))
			return
		let list = Array.from(document.querySelectorAll(selItems))
		if (!list.length)
			return

		// highlight visible elements
		list = list.filter(ele => window.getComputedStyle(ele).display != 'none')
		list.forEach(ele => ele.classList.add("tfwn-highlight"))

		// reuse the focus from the last highlight
		if (eleFocus && !eleFocus.classList.contains('tfwn-focus') && setFocus(eleFocus))
			return
		// set and view the new focus
		if (!eleFocus && setFocus(document.querySelector(seLCurrent) || list[0]))
			return

		eleFocus.classList.add("tfwn-focus")
		list = list.filter(ele => ele != eleFocus)

		// calculate next element to focus in corresponding direction
		var R = eleFocus.getBoundingClientRect()
		var args = _dir.args[dir](R)
		let ele = _findNext(dir, list, args)
		if (!ele) {
			args.a = dir == 'l' || dir == 'u' ? document.body.clientWidth : 0
			ele = _findNext(dir, list, args, 2)
		}
		setFocus(ele) // || console.debug("no element '" + dir + "' to current focus", ele)
	}


	/* - handle events */

	function clickEvent(ele, input) {
		let data = { view: window, bubbles: true, cancelable: true, ...input }
		return ele.dispatchEvent(new MouseEvent('click', data))
	}

	function switchAttribute(onOff, ele, name, value) {
		let val, tmp = name+'_tmp'
		if (onOff) {
			if (val = ele.getAttribute(name))
				ele.setAttribute(tmp, val)
			ele.setAttribute(name, value)
		} else {
			val = ele.getAttribute(tmp)
			val ? ele.setAttribute(name, val) : ele.removeAttribute(name)
			ele.removeAttribute(tmp)
		}
	}

	function applyAttributes(onOff, ele, attributes) {
		if (!attributes) return
		if (-1 < attributes.indexOf('tab')) {
			switchAttribute(onOff, ele, 'target', '_blank')
		}
	}

	function keyup(ev) {
		let tag = ev.target ? ev.target.tagName.toLowerCase() : ''
		if (['input', 'textarea'].includes(tag))
			return
		// __d_keys(ev)
		if (ev.ctrlKey || ev.shiftKey || ev.altKey)
			return

		// get readConfiged callback and click result element
		let ele, list
		if (list = keyConfig[ev.key]) {
			for (let obj of list) {
				if (ele = obj.func.apply(null, Array.isArray(obj.args) ? obj.args : [obj.args])) {
					ev.preventDefault()
					ev.stopPropagation()
					applyAttributes(true, ele, obj.attributes)
					ele.click()
					applyAttributes(false, ele, obj.attributes)
					return true
				}
			}
		}
	}

	function init() {
		window.addEventListener("keypress", keyup)
		return self
	}

})(window, document);


/***************************************************************************************** APP ****/

(function(){

	let config = `
https://www.google.com/*
	nextPrev				#pnnext								#pnprev
https://www.amazon.de/s/*
	nextPrev				a.pagnNext							a.pagnPrev
https://www.ebay.de/sch/*
	nextPrev				.pagn-next a						.pagn-prev a
https://xkcd.com/
	nextPrev				.comicNav a[rel=next]				.comicNav a[rel=prev]
http://threewordphrase.com
	nextPrev				img[src="/nextlink.gif"]			img[src="/prevlink.gif"]
https://www.marvel.com/
	nextPrev				span[data-click-text="next"]		span[data-click-text="prev"]
forums.frontier.co.uk
	nextPrev				#pagination_bottom a[rel="next"]	#pagination_bottom a[rel="prev"]
https://www.teepublic.com/*
	currentNextPrev			nav.pagination			.pagination span.page.current	span.page:not(.gap)		a
https://.elb.*com/*
	currentNextPrev			#paginator				#paginator b					a
	keyToClick				x						#resized_notice a:first-of-type
	keyToClick				t,z:tab					#tag-list li.tag-type-artist:first-of-type a:nth-of-type(2)
	navigate				.thumbnail-preview
	navigateClick			.thumbnail-preview		a	:tab
https://z0r.de*
	keyToClick				x						#disqus_button
	nextPrev				#unten a:last-of-type				#unten a:first-of-type
https://aniwatcher.com*
	currentNextPrev			#pagenavi				#pagenavi .current:not(.prev)	.page-link
	currentNextPrev			#nextprevlist			#nextprevlist .on				li:not(.center)			a
	keyToClick				b						.tfwn-focus .auto-date
	keyToClick				f						.tfwn-focus .open-dialog
	keyToClick				f						.open-dialog
	navigate				.epl2 li, #sources a	#sources a.active
	navigateClick			.epl2 li				a.ep
	navigateClick			#sources a
	n2n						#menu li				a
https://apps2.bvl.bund.de/psm/jsp/ListeMain.jsp
	nextPrev				img[src="img/vor.gif"]	img[src="img/zurueck.gif"]
	navigate				#tabPsm td:nth-of-type(1), #tabPsm td:nth-of-type(2)
	navigateClick			#tabPsm td:nth-of-type(1), #tabPsm td:nth-of-type(2)	a	:tab
`

	let nav = window.Navigation.init()
	let funcs = {
		keyToClick: function(keys, sel, cb) {
			nav.keyToClick(keys, cb ? cb : nav.querySelector, sel)
		},
		nextPrev: function(selNext, selPrev) {
			nav.keyToClick('q', nav.querySelector, selPrev)
			nav.keyToClick('e', nav.querySelector, selNext)
		},
		currentNextPrev: function(selList, selCurr, selItems, selSub) {
			nav.keyToClick('q', nav.prevPaginationItem, [selList, selCurr, selItems, selSub])
			nav.keyToClick('e', nav.nextPaginationItem, [selList, selCurr, selItems, selSub])
		},
		navigate: function(selItems, seLCurrent) {
			nav.keyToClick('a', nav.navigate, ["l", selItems, seLCurrent])
			nav.keyToClick('d', nav.navigate, ["r", selItems, seLCurrent])
			nav.keyToClick('w', nav.navigate, ["u", selItems, seLCurrent])
			nav.keyToClick('s', nav.navigate, ["d", selItems, seLCurrent])
			nav.keyToClick('Escape', nav.removeHighlight)
		},
		navigateClick: function(selItems, selSub, ext) {
			nav.keyToClick(' '+(ext ? ext : ''), nav.queryFocus, [selItems, selSub])
		},
		n2n: function(selItems, selSub, max) {
			if (!max || max > 9) max = 9
			for (let i=1; i<=max; i++) {
				nav.keyToClick(''+i, nav.querySelector, [selItems+':nth-of-type('+i+')', selSub])
			}
		},
	}

	function readConfig(config) {
		let url, args, l
		for (let line of config.split('\n')) {
			l = line.trim()
			if (!l.length)
				continue
			if (/^\s/.test(line)) {
				if (!url) continue
				try {
					args = l.split(/\t+/g)
					func = args.shift()
					if (funcs[func]) {
						funcs[func].apply(null, args)
					} else {
						console.warn("'"+func+"'", 'is not a valid function name', Object.keys(funcs))
					}
				} catch(e) {
					console.error('Failed to run:', func, args)
				}
			} else {
				url = matchURL(l.trim())
			}
		}
	}

	function keyup(key) {
		nav.keyup(new KeyboardEvent('keyup', { target: document.body, key:key }))
	}

	function init() {
		// keyup('d')
	}

	readConfig(config)

	let self = window.NavigationApp = {}
	;[config,funcs,init].forEach(f => self[f.name] = f)

}());

/* 
- navigate overlap to other direction
- shift+alt+f -> focus search field
- fix document scroll
*/