// ==UserScript==
// @author              adair
// @namespace           adair.plex-download
// @name                plex-download
// @include
// @description         Download files directly from the Plex web interface
// @run-at              document-idle
// @grant               none
// @noframes
// ==/UserScript==

var plxMltDwnld = (function() {
    var self = {}
    var clientIdRegex    = new RegExp("server\/([a-f0-9]{40})\/")
    var metadataIdRegex  = new RegExp("key=%2Flibrary%2Fmetadata%2F(\\d+)")
    var apiResourceUrl   = "https://plex.tv/api/resources?includeHttps=1&X-Plex-Token={token}"
    var apiLibraryUrl    = "{baseuri}/library/metadata/{id}?X-Plex-Token={token}"
    var downloadUrl      = "{baseuri}{partkey}?download=1&X-Plex-Token={token}"
    var accessTokenXpath = "//Device[@clientIdentifier='{clientid}']/@accessToken"
    var baseUriXpath     = "//Device[@clientIdentifier='{clientid}']/Connection[@local=0]/@uri"
    var partKeyXpath     = "//Media/Part[1]/@key"
    var baseUri     = null
    var accessToken = null

    /**
     * Async get request.
     * @param url      Request url.
     * @param callback Callback function with xml data as parameter.
     */
    var getXml = function(url, callback) {
        console.debug('GETXML', url)
        var request = new XMLHttpRequest()
        request.onreadystatechange = function() {
            console.debug('GETXML 2', request.readyState, request.status)
            if (request.readyState == 4 && request.status == 200) {
                callback(request.responseXML)
            }
        }
        request.open("GET", url)
        request.send()
    }

    /**
     * Generate access token from Plex resource xml.
     * @param xml Plex resource xml data.
     */
    var getMetadata = function(xml) {
        var clientId = clientIdRegex.exec(window.location.href)

        if (clientId && clientId.length == 2) {
            console.debug("GETMETADATA 1", window.location.href, clientId[1])
            var accessTokenNode = xml.evaluate(accessTokenXpath.replace('{clientid}', clientId[1]), xml, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null)
            var baseUriNode = xml.evaluate(baseUriXpath.replace('{clientid}', clientId[1]), xml, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null)

            console.debug("GETMETADATA 2", accessTokenNode)
            if (accessTokenNode.singleNodeValue && baseUriNode.singleNodeValue) {
                accessToken = accessTokenNode.singleNodeValue.textContent
                baseUri = baseUriNode.singleNodeValue.textContent
                var accessUrl = apiLibraryUrl.replace('{baseuri}', baseUri).replace('{token}', accessToken)
                handleDownloads(accessUrl)
            } else {
                alert("Cannot find a valid accessToken.")
            }
        } else {
            alert("You are currently not viewing a media item. 1")
        }
    }

    /**
     * Reuses if exists, else creates a HTML section in the sidebar for holding download links.
     * @param accessUrl Valid Plex base URL with token and key.
     */
    var handleDownloads = function(accessUrl) {
        var items = document.querySelectorAll("[data-qa-id^=metadataTitleContainer]")
        console.debug(items)
        if (!items.length) {
            getDownloadUrl(accessUrl, window.location.href, function(downloadUrl) {
                console.debug('GETDOWNLOADURL CB', downloadUrl)
                window.location.href = downloadUrl
            })
            return;
        }

        htmlHolder(true)
        for (var i=0; i<items.length; i++) {
            let span = items[i].getElementsByTagName('span')[0]
            let link = items[i].getElementsByTagName('a')[0]
            let title = span.innerHTML+' '+link.innerHTML

            htmlDownloadLink(title)
            getDownloadUrl(accessUrl, link.href, function(downloadUrl) {
                htmlDownloadLink(title, downloadUrl)
                console.debug('GETDOWNLOADURL CB 2', downloadUrl)
            })
        }
    }

    /**
     * Reuses if exists, else creates a HTML section in the sidebar for holding download links.
     * @param accessUrl Valid Plex base URL with token and key.
     * @param url       Web page URL retrieve the download URL from.
     * @param callback  Callback function with the download URL as parameter.
     */
    var getDownloadUrl = function(accessUrl, url, callback) {

        var metadataId = metadataIdRegex.exec(url)

        if (metadataId && metadataId.length == 2) {
console.debug("GETDOWNLOADURL", accessUrl.replace('{id}', metadataId[1]))
            getXml(accessUrl.replace('{id}', metadataId[1]), function(xml) {
                console.debug('GETDOWNLOADURL CB', xml)
                var partKeyNode = xml.evaluate(partKeyXpath, xml, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null)

                if (partKeyNode.singleNodeValue) {
                    callback(downloadUrl.replace('{baseuri}', baseUri).replace('{partkey}', partKeyNode.singleNodeValue.textContent).replace('{token}', accessToken))
                } else {
                    alert("You are currently not viewing a media item. 3")
                }
            })
        } else {
            alert("You are currently not viewing a media item. 2")
        }
    }

    /**
     * Reuses if exists, else creates a HTML section in the sidebar for holding download links.
     * @param reset Clears the holder if true.
     * @return HTML DOM element of the holder.
     */
    var htmlHolder = function(reset) {
        var target = document.querySelectorAll("[class^=SidebarScroller-scroller-]")
        console.debug('HTMLHOLDER', target)
        if (!target.length) {
            return null
        }

        var ele = document.getElementById('plxmltdwnld-header')
        if (!ele) {
            target[0].innerHTML +=
            '<h5 id="plxmltdwnld-header">Download Links</h5>'
        }

        var ele = document.getElementById('plxmltdwnld-holder')
        if (!ele) {
            target[0].innerHTML +=
            '<div id="plxmltdwnld-holder" style="width:230px; height:600px; padding:1px; overflow:hidden; overflow-y:auto; "></div>'
        } else if (reset) {
            ele.innerHTML = ''
        }
        return ele
    }

    /**
     * Updates if exists, else creates a HTML placeholder for a download link.
     * @param title The placeholder title and text.
     * @param url   The download URL, which when given updates the element.
     */
    var htmlDownloadLink = function(title, url) {
        if (url) {
            var link = document.querySelectorAll('[title="'+title+'"]')
            if (link.length) {
                link[0].href = url
                link[0].innerHTML = title
            }
        } else {
            var holder = htmlHolder()
            if (holder) {
                holder.innerHTML += '<a title="'+title+'" style="white-space:nowrap;">-</a></br>'
            }
        }
    }

    /**
     * Init function.
     */
    self.init = function() {
        console.debug('#############################')
        if (typeof localStorage.myPlexAccessToken != "undefined") {
            getXml(apiResourceUrl.replace('{token}', localStorage.myPlexAccessToken), getMetadata)
        }
    }

    return self
}())

setTimeout(plxMltDwnld.init, 5000)
// plxMltDwnld.init();